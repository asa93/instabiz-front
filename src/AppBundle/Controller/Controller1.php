<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManagerInterface;
use AppBundle\Entity\Product;

class Controller1 extends Controller
{
    /**
     * @Route("/numero")
     */
    public function numeroAction()
    {
	$entityManager = $this->getDoctrine()->getManager();

    $product = new Product();
	$product->setNom('toto');
	$product->setPrenom('tata');
	$product->setAdresse('Momo');
    $product->setVille('mama');
    // tells Doctrine you want to (eventually) save the Product (no queries yet)
    $entityManager->persist($product);

    // actually executes the queries (i.e. the INSERT query)
    $entityManager->flush();
	
        $html = $this->render('index.html.twig',array('nom' => $product->getNom(),'prenom' => $product->getPrenom(),'adresse' => $product->getAdresse(),'ville' => $product->getVille()));

        return new Response($html);
    }
	
	 /**
     * @Route("/services")
     */
    public function serviceAction()
    {

        $html = $this->render('index.html.twig');

        return new Response($html);
    }
}