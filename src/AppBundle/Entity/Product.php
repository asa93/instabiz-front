<?php namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="foo")
 */
class Product
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
	 private $id;
	 
   /**
     * @ORM\Column(type="string", length=50)
     */
    private $nom;

     /**
     * @ORM\Column(type="string", length=50)
     */
    private $prenom;

     /**
     * @ORM\Column(type="string", length=50)
     */
	 private $adresse;
	 /**
     * @ORM\Column(type="string", length=50)
     */
	 private $ville;

	 public function getNom(){
		 return $this->nom;
	 }
	 public function getPrenom(){
		 return $this->prenom;
	 }
	 public function getAdresse(){
		 return $this->adresse;
	 }
	 public function getVille(){
		 return $this->ville;
	 }
	 public function setNom($v){
		 $this->nom=$v;
	 }
	 public function setPrenom($v){
		 $this->prenom=$v;
	 }
	 public function setAdresse($v){
		 $this->adresse=$v;
	 }
	 public function setVille($v){
		 $this->ville=$v;
	 }
	 
}